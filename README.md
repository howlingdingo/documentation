# Documentation

This project is the wiki / documentation for the Howling Dingo projects.

Howling Dingo is an attempt to go from 0 to a small working microservice based web application in a couple of days.

It is designed to be able to be run locally as a collection of node services and subsequently deployed to a local micro k8s instance and eventually to either GKE or some other alternative.

Initially the choice for the microservices will be node running express, and with two UIs running react and angular.

The backend will be persisted to a PostgreSQL database.

GraphQL will be used as the external API.

The initial scope of the project is a simple CRUD application for a todo list, but themed around a 'garden' management application. The actual domain of the application is less relevant than the ideas that are being developed.
